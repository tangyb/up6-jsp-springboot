package com.ncmem.down2.database.mongo;

import com.google.gson.Gson;
import com.mongodb.client.result.UpdateResult;
import com.ncmem.down2.database.sql.DnFile;
import com.ncmem.up6.model.DnFileInf;
import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.SpringUtil;
import com.ncmem.up6.model.FileInf;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class DnFileMongodb extends DnFile {

    private MongoTemplate mongo;

    public DnFileMongodb(){
        this.mongo = SpringUtil.getBean(MongoTemplate.class);
    }

    public void Add(DnFileInf inf)
    {
        this.mongo.insert(inf,"down_files");
    }

    /**
     * 将文件设为已完成
     * @param fid
     */
    public void Complete(String fid)
    {
        Query q = new Query(Criteria.where("id").is(fid));
        List<DnFileInf> fs = this.mongo.find(q,DnFileInf.class);
    }

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="fid"></param>
    public void Delete(String fid,int uid)
    {
        Query q = new Query(Criteria.where("id").is(fid));
        this.mongo.remove(q,"down_files");
    }

    /**
     * 更新文件进度信息
     * @param fid
     * @param uid
     * @param lenLoc
     */
    public void process(String fid,int uid,String lenLoc,String perLoc)
    {

        Query query = new Query(Criteria.where("id").is(fid));
        Update up = new Update();
        up.set("lenLoc",lenLoc);
        up.set("perLoc",perLoc);
        UpdateResult result = this.mongo.upsert(query, up, DnFileInf.class,"down_files");
    }

    /// <summary>
    /// 获取所有未下载完的文件列表
    /// </summary>
    /// <returns></returns>
    public String all_uncmp(int uid)
    {
        List<DnFileInf> fs = this.mongo.findAll(DnFileInf.class,"down_files");

        Gson g = new Gson();
        String js = g.toJson( fs );
        System.out.println("all_uncmp");
        System.out.println(js);
        return js;
    }

    /**
     * 从up6_files表中获取已经上传完的数据
     * @param uid
     * @return
     */
    public String all_complete(int uid)
    {
        Query q = new Query(Criteria.where("complete").is(true));
        List<FileInf> fs = this.mongo.find(q,FileInf.class,"up6_files");
        List<DnFileInf> files = new ArrayList<DnFileInf>();
        for(FileInf s : fs)
        {
            DnFileInf f = new DnFileInf();
            f.id = f.makeID();
            f.f_id = s.id;
            f.fdTask = s.fdTask;
            f.nameLoc = s.nameLoc;
            f.sizeSvr = s.sizeLoc;
            f.lenSvr = s.lenLoc;
            f.pathSvr = s.pathSvr;
            f.lenLocSec = s.lenLocSec;
            f.encrypt = s.encrypt;
            f.object_key = PathTool.url_safe_encode( s.object_key);

            files.add(f);
        }

        Gson g = new Gson();
        String js = g.toJson( files );
        System.out.println("all_complete");
        System.out.println(js);
        return js;
    }

    public void Clear()
    {
        Query q = new Query(Criteria.where("complete").is(false));
        this.mongo.findAllAndRemove(q,DnFileInf.class);
    }
}
