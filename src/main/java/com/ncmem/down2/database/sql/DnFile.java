package com.ncmem.down2.database.sql;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.ncmem.down2.database.mongo.DnFileMongodb;
import com.ncmem.down2.database.obdc.DnFileOdbc;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.sql.*;
import com.ncmem.up6.utils.ConfigReader;
import com.ncmem.up6.utils.DataBaseType;
import com.ncmem.up6.utils.PathTool;

import com.google.gson.Gson;
import com.ncmem.up6.model.DnFileInf;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class DnFile 
{
	public DnFile()
	{
	}

	public static DnFile build(){
		DataBaseType tp = ConfigReader.dbType();
		if( tp == DataBaseType.KingBase ) return new DnFileOdbc();
		else if( tp == DataBaseType.MongoDB ) return new DnFileMongodb();
		else return new DnFile();
	}

    public void Add(DnFileInf inf) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").insert(inf);
    }

    /**
     * 将文件设为已完成
     * @param fid
     */
    public void Complete(String fid) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").update(
    			SqlSeter.build().set("f_complete",true),
				SqlWhere.build().eq("f_id",fid)
		);
    }

    /// <summary>
    /// 删除文件
    /// </summary>
    /// <param name="fid"></param>
    public void Delete(String fid,int uid) throws SQLException, ParseException {
    	SqlTable.build("down_files").del(
    			SqlWhere.build()
						.eq("f_id",fid)
						.eq("f_uid",uid)
		);
    }
    
    /**
     * 更新文件进度信息
     * @param fid
     * @param uid
     * @param lenLoc
     */
    public void process(String fid,int uid,String lenLoc,String perLoc) throws ParseException, IllegalAccessException, SQLException {
    	SqlTable.build("down_files").update(
    			SqlSeter.build()
						.set("f_lenLoc",Long.parseLong(lenLoc))
						.set("f_perLoc",perLoc),
				SqlWhere.build()
						.eq("f_id",fid)
				.eq("f_uid",uid)
		);
    }

    /// <summary>
    /// 获取所有未下载完的文件列表
    /// </summary>
    /// <returns></returns>
    public String all_uncmp(int uid) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
    	List<DnFileInf> fs = SqlTable.build("down_files").reads(DnFileInf.build(),
				SqlWhere.build()
						.eq("f_complete",false)
						.eq("f_uid",uid)
		);

        Gson g = new Gson();
	    return g.toJson( fs );
	}
    
    /**
     * 从up6_files表中获取已经上传完的数据
     * @param uid
     * @return
     */
    public String all_complete(int uid) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
		List<DnFileInf> files = SqlTable.build("up6_files").reads(DnFileInf.build(),
				"f_id,f_fdTask,f_nameLoc,f_sizeLoc,f_lenSvr,f_pathSvr,f_lenLocSec,f_encrypt,f_blockSize,f_object_key",
				SqlWhere.build()
						.eq("f_uid",uid)
						.eq("f_deleted",false)
						.eq("f_complete",true)
						.eq("f_fdChild",false)
						.eq("f_scan",true)
		);

		for (DnFileInf f : files)
		{
			f.f_id = f.id;//
			f.id = f.makeID();
			f.sizeSvr = f.sizeLoc;
			f.sizeLoc="0byte";
			f.lenLoc=0;
			f.pathLoc = "";
			f.object_key	= PathTool.url_safe_encode( f.object_key );
		}
        Gson g = new Gson();
	    return g.toJson( files );
    }
    
    public void Clear()
    {
    	SqlTable.build("down_files").clear();
    }

	/**
	 * 取所有子文件
	 * @param pidRoot
	 * @return
	 */
	public String childs(String pidRoot) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
		List<FileInf> files = SqlTable.build("up6_files").reads(FileInf.build(),
				SqlWhere.build().eq("f_pidRoot",pidRoot)
		);
		List<JSONObject> arr = new ArrayList<JSONObject>();
		for (FileInf f : files)
		{
			JSONObject o = JSONObject.fromObject(f);
			o.put("f_id",f.id);
			JSONObject fields = new JSONObject();
			fields.put("object_key",PathTool.url_safe_encode(f.object_key));
			o.put("fields",fields);
			arr.add(o);
		}
		return  JSONArray.fromObject(arr).toString();
	}
}