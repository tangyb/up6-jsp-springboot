package com.ncmem.controller;

import com.ncmem.up6.store.fastdfs.FastDFSTool;
import org.csource.fastdfs.FileInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 2021/1/6.
 */
@Controller
public class Up6Ui {
    @Autowired
    private HttpServletRequest req;

    @Autowired
    private HttpServletResponse res;

    @RequestMapping("up6/index")
    public String up6(Model model)
    {
        return "up6/index";
    }

    @RequestMapping("up6/nosql/index")
    public String up6_nosql(Model model)
    {
        return "up6/nosql/index";
    }

    /**
     * 单面板示例
     * @param model
     * @return
     */
    @RequestMapping("up6/panel")
    public String panel(Model model) {

        return "up6/panel";
    }

    @RequestMapping("up6/nosql/panel")
    public String panel_nosql(Model model) {

        return "up6/nosql/panel";
    }

    @RequestMapping("up6/single")
    public String single(Model model)
    {
        return "up6/single";
    }

    @RequestMapping("up6/vue")
    public String vue(Model model) {
        return "up6/vue";
    }

    @RequestMapping("up6/layer")
    public String layer(Model model) {
        return "up6/layer";
    }

    @RequestMapping("up6/upload")
    public String upload() {
        return "up6/test";
    }

    @RequestMapping("up6/index1")
    public String test() {
       FileInfo f= FastDFSTool.query("group1/M00/00/04/wKgAb2J438-ETnPvAAAAAAAAAAA781.txt");
       System.out.println(f.getFileSize());
        return "up6/index";
    }
}
