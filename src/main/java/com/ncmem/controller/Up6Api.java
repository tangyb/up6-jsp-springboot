package com.ncmem.controller;

import com.google.gson.Gson;
import com.ncmem.up6.biz.*;
import com.ncmem.up6.database.DBConfig;
import com.ncmem.up6.database.DBFile;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.sql.SqlTable;
import com.ncmem.up6.store.FileBlockWriter;
import com.ncmem.up6.utils.*;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * * Created by jmzy on 2021/1/6.
 * 接口：
 * up6/clear
 * up6/f_create
 * up6/f_post
 * up6/f_process
 * up6/f_complete
 * up6/f_del
 * up6/f_list
 * up6/fd_create
 * up6/fd_complete
 * up6/fd_del
 * 注意：
 * 1.除f_post外其它接口均由前端(up6.js,up6.file.js,up6.folder.js)自动调用
 * 2.f_post由控件自动调用。
 */
@RestController
public class Up6Api {
    @Autowired
    private HttpServletRequest req;

    @Autowired
    private HttpServletResponse res;

    @GetMapping("up6/test")
    public String test() throws Exception {

        FileInf f = new FileInf();
        f.nameLoc="测试数据";
        f.id="abcdefg";
        SqlTable t = new SqlTable("up6_files");
        t.insert(f);
        return "";
    }

    @GetMapping("up6/getRoot")
    public String getRoot() throws IOException {
        return ResTool.readFile("config/config.json");
    }

    /**
     * 清空数据库，由前端调用
     * @return
     */
    @RequestMapping(value="up6/clear",method = RequestMethod.GET)
    public String clear()
    {
        DBFile.build().clear();

        return "数据库清理完毕";
    }

    /**
     * 文件初始化,在文件上传前调用，在数据表中记录文件信息。由前諯调用
     * 调用位置：up6.file.js-md5_complete
     * @param id 文件ID，由控件生成，GUID
     * @param md5，文件MD5，由控件计算。
     * @param uid，用户ID，在JS中配置
     * @param lenLoc，数字格式的文件大小，控件提供
     * @param sizeLoc 格式化的文件大小，控件提供
     * @param callback JQ回调方法名称
     * @param pathLoc 文件本地路径，控件提供
     * @return
     */
    @RequestMapping(value="up6/f_create",method = RequestMethod.GET)
    public String f_create(@RequestParam(value="id", required=false,defaultValue="")String id,
                           @RequestParam(value="md5", required=false,defaultValue="")String md5,
                           @RequestParam(value="uid", required=false,defaultValue="")String uid,
                           @RequestParam(value="lenLoc", required=false,defaultValue="")String lenLoc,
                           @RequestParam(value="sizeLoc", required=false,defaultValue="")String sizeLoc,
                           @RequestParam(value="blockSize", required=false,defaultValue="0")String blockSize,
                           @RequestParam(value="token", required=false,defaultValue="")String token,
                           @RequestParam(value="callback", required=false,defaultValue="")String callback,
                           @RequestParam(value="pathLoc", required=false,defaultValue="")String pathLoc) throws ParseException, IllegalAccessException, SQLException {

        pathLoc	= PathTool.url_decode(pathLoc);

        //参数为空
        if (	StringUtils.isBlank(md5)
                && StringUtils.isBlank(uid)
                && StringUtils.isBlank(sizeLoc))
        {
            return (callback + "({\"value\":null})");
        }

        FileInf fileSvr= new FileInf();
        fileSvr.id = id;
        fileSvr.fdChild = false;
        fileSvr.uid = Integer.parseInt(uid);
        fileSvr.nameLoc = PathTool.getName(pathLoc);
        fileSvr.nameSvr = fileSvr.nameLoc;
        fileSvr.pathLoc = pathLoc;
        fileSvr.lenLoc = Long.parseLong(lenLoc);
        fileSvr.calLenLocSec();//计算文件加密后的大小
        fileSvr.sizeLoc = sizeLoc;
        fileSvr.deleted = false;
        fileSvr.md5 = md5;
        fileSvr.blockSize=Integer.parseInt(blockSize);
        fileSvr.encrypt = ConfigReader.storageEncrypt();//加密存储

        WebSafe ws = new WebSafe();
        boolean ret = ws.validToken(token, fileSvr);
        if(!ret)
        {
            return (callback + "({\"value\":\"0\",\"ret\":false,\"msg\":\"token error\"})");
        }

        //所有单个文件均以uuid/file方式存储
        PathBuilderUuid pb = new PathBuilderUuid();
        try {
            fileSvr.pathSvr = pb.genFile(fileSvr.uid,fileSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");

        DBFile db = DBFile.build();
        FileInf fileExist = db.exist_file(md5);

        //数据库已存在相同文件，且有上传进度，则直接使用此信息
        if(null!=fileExist)
        {
            fileSvr.nameSvr			= fileExist.nameSvr;
            fileSvr.pathSvr 		= fileExist.pathSvr;
            fileSvr.perSvr 			= fileExist.perSvr;
            fileSvr.lenSvr 			= fileExist.lenSvr;
            fileSvr.complete		= fileExist.complete;
            fileSvr.encrypt = fileExist.encrypt;
            fileSvr.lenLocSec = fileExist.lenLocSec;
            fileSvr.blockSize = fileExist.blockSize;
            fileSvr.object_id = fileExist.object_id;
            fileSvr.object_key = fileExist.object_key;
            db.Add(fileSvr);

            //触发事件
            up6_biz_event.file_create_same(fileSvr);
        }//此文件不存在
        else
        {
            try {
                FileBlockWriter fw = ConfigReader.blockWriter();
                fileSvr.object_id = fw.make(fileSvr);
                fileSvr.object_key = fileSvr.getObjectKey();
            } catch (IOException e) {
                e.printStackTrace();
                res.setStatus(500);
                return e.getMessage();
            }

            db.Add(fileSvr);
            //触发事件
            up6_biz_event.file_create(fileSvr);
        }

        //将路径转换成相对路径
        try {
            fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //传输加密
        if (ConfigReader.postEncrypt())
        {
            CryptoTool ct   = new CryptoTool();
            try {
                fileSvr.pathSvr = ct.encrypt(fileSvr.pathSvr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(fileSvr);

        try {
            json = URLEncoder.encode(json,"UTF-8");//编码，防止中文乱码
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        json = json.replace("+","%20");
        json = callback + "({\"value\":\"" + json + "\",\"ret\":true})";//返回jsonp格式数据。
        return json;
    }

    @RequestMapping(value="up6/nosql/f_create",method = RequestMethod.GET)
    public String f_create_nosql(@RequestParam(value="id", required=false,defaultValue="")String id,
                           @RequestParam(value="md5", required=false,defaultValue="")String md5,
                           @RequestParam(value="uid", required=false,defaultValue="")String uid,
                           @RequestParam(value="lenLoc", required=false,defaultValue="")String lenLoc,
                           @RequestParam(value="sizeLoc", required=false,defaultValue="")String sizeLoc,
                           @RequestParam(value="blockSize", required=false,defaultValue="0")String blockSize,
                           @RequestParam(value="token", required=false,defaultValue="")String token,
                           @RequestParam(value="callback", required=false,defaultValue="")String callback,
                           @RequestParam(value="pathLoc", required=false,defaultValue="")String pathLoc) {

        pathLoc	= PathTool.url_decode(pathLoc);

        //参数为空
        if (	StringUtils.isBlank(md5)&&
                StringUtils.isBlank(uid)&&
                StringUtils.isBlank(sizeLoc))
        {
            return (callback + "({\"value\":null})");
        }

        FileInf fileSvr= new FileInf();
        fileSvr.id = id;
        fileSvr.fdChild = false;
        fileSvr.uid = Integer.parseInt(uid);
        fileSvr.nameLoc = PathTool.getName(pathLoc);
        fileSvr.nameSvr = fileSvr.nameLoc;
        fileSvr.pathLoc = pathLoc;
        fileSvr.lenLoc = Long.parseLong(lenLoc);
        fileSvr.calLenLocSec();//计算文件加密后的大小
        fileSvr.sizeLoc = sizeLoc;
        fileSvr.deleted = false;
        fileSvr.md5 = md5;
        fileSvr.blockSize=Integer.parseInt(blockSize);
        fileSvr.encrypt = ConfigReader.storageEncrypt();//加密存储

        WebSafe ws = new WebSafe();
        boolean ret = ws.validToken(token, fileSvr);
        if(!ret)
        {
            return (callback + "({\"value\":\"0\",\"ret\":false,\"msg\":\"token error\"})");
        }

        //所有单个文件均以uuid/file方式存储
        PathBuilderUuid pb = new PathBuilderUuid();
        try {
            fileSvr.pathSvr = pb.genFile(fileSvr.uid,fileSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");

        DBConfig cfg = new DBConfig();
        DBFile db = cfg.db();

        FileBlockWriter fw = ConfigReader.blockWriter();
        try {
            fileSvr.object_id  = fw.make(fileSvr);
            fileSvr.object_key = fileSvr.getObjectKey();
        } catch (IOException e) {
            e.printStackTrace();
            res.setStatus(500);
            return  e.getMessage();
        }

        //触发事件
        up6_biz_event.file_create(fileSvr);

        //将路径转换成相对路径
        try {
            fileSvr.pathSvr = pb.absToRel(fileSvr.pathSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //传输加密
        if (ConfigReader.postEncrypt())
        {
            CryptoTool ct   = new CryptoTool();
            try {
                fileSvr.pathSvr = ct.encrypt(fileSvr.pathSvr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(fileSvr);

        try {
            json = URLEncoder.encode(json,"UTF-8");//编码，防止中文乱码
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        json = json.replace("+","%20");
        json = callback + "({\"value\":\"" + json + "\",\"ret\":true})";//返回jsonp格式数据。
        return json;
    }

    /**
     * 文件上传完毕，在文件上传完毕后由前端调用。
     * 调用位置：up6.file.js-post_complete
     * @param id 文件ID，由控件生成，GUID 示例：04fdfc13fc2a4b7a9d0682f8c344926f
     * @param uid 用户ID，在JS中困于
     * @param md5 文件MD5
     * @param pid 文件PID
     * @param callback JQ回调
     * @param nameLoc 文件名称，由控件提供
     * @return
     */
    @RequestMapping(value="up6/f_complete",method = RequestMethod.GET)
    public String f_complete(@RequestParam(value="id", required=false,defaultValue="")String id,
                             @RequestParam(value="uid", required=false,defaultValue="0")Integer uid,
                             @RequestParam(value="md5", required=false,defaultValue="")String md5,
                             @RequestParam(value="pid", required=false,defaultValue="")String pid,
                             @RequestParam(value="callback", required=false,defaultValue="")String callback,
                             @RequestParam(value="nameLoc", required=false,defaultValue="")String nameLoc) throws ParseException, IllegalAccessException, SQLException {
        nameLoc	= PathTool.url_decode(nameLoc);
        //返回值。1表示成功
        int ret = 0;
        if ( !StringUtils.isBlank(id))
        {
            DBFile.build().complete(id,uid);

            up6_biz_event.file_post_complete(id);
            ret = 1;
        }
        return callback + "(" + ret + ")";
    }

    @RequestMapping(value="up6/nosql/f_complete",method = RequestMethod.GET)
    public String f_complete_nosql(@RequestParam(value="id", required=false,defaultValue="")String id,
                             @RequestParam(value="uid", required=false,defaultValue="0")Integer uid,
                             @RequestParam(value="md5", required=false,defaultValue="")String md5,
                             @RequestParam(value="pid", required=false,defaultValue="")String pid,
                             @RequestParam(value="callback", required=false,defaultValue="")String callback,
                             @RequestParam(value="nameLoc", required=false,defaultValue="")String nameLoc)
    {
        nameLoc	= PathTool.url_decode(nameLoc);
        //返回值。1表示成功
        int ret = 0;
        if ( !StringUtils.isBlank(id))
        {
            up6_biz_event.file_post_complete(id);
            ret = 1;
        }
        return callback + "(" + ret + ")";
    }

    /**
     * 删除文件,在文件列表中调用，由前端调用。
     * 调用位置，up6.js
     * @param id 文件ID
     * @param uid 用户ID
     * @param callback JQ回调方法，用于支持跨域调用
     * @return
     */
    @RequestMapping(value="up6/f_del",method = RequestMethod.GET)
    @ResponseBody
    public String f_del(@RequestParam(value="id", required=false,defaultValue="")String id,
                             @RequestParam(value="uid", required=false,defaultValue="0")String uid,
                             @RequestParam(value="callback", required=false,defaultValue="")String callback
                             ) throws ParseException, IllegalAccessException, SQLException {
        //返回值。1表示成功
        int ret = 0;

        if (!StringUtils.isBlank(id) &&
            !StringUtils.isBlank(uid))
        {
            DBConfig cfg = new DBConfig();
            DBFile db = cfg.db();
            db.Delete(Integer.parseInt(uid),id);
            up6_biz_event.file_del(id,Integer.parseInt(uid));
            ret = 1;
        }
        return callback + "(" + ret + ")";
    }

    @RequestMapping(value="up6/nosql/f_del",method = RequestMethod.GET)
    @ResponseBody
    public String f_del_nosql(@RequestParam(value="id", required=false,defaultValue="")String id,
                        @RequestParam(value="uid", required=false,defaultValue="0")String uid,
                        @RequestParam(value="callback", required=false,defaultValue="")String callback
    )
    {
        //返回值。1表示成功
        int ret = 0;

        if (!StringUtils.isBlank(id) &&
                !StringUtils.isBlank(uid))
        {
            up6_biz_event.file_del(id,Integer.parseInt(uid));
            ret = 1;
        }
        return callback + "(" + ret + ")";
    }

    /**
     * 加载未上传完的文件列表，在文件列表中调用，由前端调用。
     * 加载上传面板时自动调用。
     * 调用位置：up6.js
     * @param uid 用户UI
     * @param cbk JQ回调方法，提供跨域调用
     * @return
     */
    @RequestMapping(value="up6/f_list",method = RequestMethod.GET)
    public String f_list(@RequestParam(value="uid", required=false,defaultValue="0")String uid,
                        @RequestParam(value="callback", required=false,defaultValue="")String cbk
                        ) throws SQLException, InstantiationException, ParseException, IllegalAccessException {
        if( uid.length() > 0 )
        {
            DBConfig cfg = new DBConfig();
            String json = cfg.db().unCompletes( Integer.parseInt( uid ) );
            if( !StringUtils.isBlank(json))
            {
                try {
                    json = URLEncoder.encode(json,"utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                json = json.replace("+","%20");
                return  cbk + "({\"value\":\""+json + "\"})" ;

            }
        }
        return (cbk + "({\"value\":null})");
    }

    @RequestMapping(value="up6/nosql/f_list",method = RequestMethod.GET)
    public String f_list_nosql(@RequestParam(value="uid", required=false,defaultValue="0")String uid,
                         @RequestParam(value="callback", required=false,defaultValue="")String cbk
    )
    {
        return (cbk + "({\"value\":null})");
    }

    /**
     * 上传文件块，由控件调用
     * @param uid 用户ID
     * @param id 文件ID
     * @param lenSvr 远程文件大小，由控件提供
     * @param lenLoc 本地文件大小，由控件提供
     * @param blockOffset 块偏移位置（相对于整个文件），由控件提供
     * @param blockSize 原始块大小，由控件提供
     * @param blockSizeCry 加密块大小，由控件提供
     * @param blockEncrypt 是否已经加密，由控件提供
     * @param blockIndex 块索引，基于1，由控件提供
     * @param blockMd5 块MD5，由控件提供
     * @param complete 是否是最后一块，由控件提供
     * @param pathSvr 远程文件路径（文件保存位置），由控件提供
     * @param thumb 缩略图，由控件提供
     * @param blockData 块数据，由控件提供
     * @return
     */
    @RequestMapping(value="up6/f_post",method = RequestMethod.POST)
    public String f_post(@RequestHeader(value="uid", required=false,defaultValue="")String uid,
                         @RequestHeader(value="id", required=false,defaultValue="")String id,
                         @RequestHeader(value="pid", required=false,defaultValue="")String pid,
                         @RequestHeader(value="pidRoot", required=false,defaultValue="")String pidRoot,
                         @RequestHeader(value="lenSvr", required=false,defaultValue="0")String lenSvr,
                         @RequestHeader(value="lenLoc", required=false,defaultValue="0")String lenLoc,
                         @RequestHeader(value="blockOffset", required=false,defaultValue="")String blockOffset,
                         @RequestHeader(value="blockSize", required=false)int blockSize,
                         @RequestHeader(value="blockSizeCry", required=false,defaultValue = "0")int blockSizeCry,
                         @RequestHeader(value="blockEncrypt", required=false,defaultValue = "false")Boolean blockEncrypt,
                         @RequestHeader(value="blockIndex", required=false,defaultValue="")String blockIndex,
                         @RequestHeader(value="blockCount", required=false,defaultValue="")String blockCount,
                         @RequestHeader(value="blockMd5", required=false,defaultValue="")String blockMd5,
                         @RequestHeader(value="complete", required=false,defaultValue="")String complete,
                         @RequestHeader(value="object_id", required=false,defaultValue="")String object_id,
                         @RequestHeader(value="token",required=false,defaultValue="") String token,
                         @RequestParam(value="pathSvr",required=false,defaultValue="") String pathSvr,
                         @RequestParam(value="pathRel",required=false,defaultValue="") String pathRel,
                         @RequestParam(value="pathLoc",required=false,defaultValue="") String pathLoc,
                         @RequestParam(value="thumb",required=false) MultipartFile thumb,
                         @RequestParam(value="file",required=true) MultipartFile blockData
                         )
    {
        boolean isLastBlock = StringUtils.equals(complete, "true");
        //参数为空
        if(	 StringUtils.isEmpty( uid )||
                StringUtils.isBlank( id )||
                StringUtils.isEmpty( blockOffset ))
        {
            return "uid,id,blockOffset empty";
        }
        pathSvr = PathTool.url_decode(pathSvr);
        pathRel = PathTool.url_decode(pathRel);
        pathLoc = PathTool.url_decode(pathLoc);

        ByteArrayOutputStream ostm = StreamTool.toStream(blockData);
        if(StringUtils.isBlank( pathLoc )) pathLoc = blockData.getOriginalFilename();

        FileInf fileSvr = new FileInf();
        fileSvr.id = id;
        fileSvr.pid=pid;
        fileSvr.pidRoot=pidRoot;
        fileSvr.object_id=object_id;
        fileSvr.lenSvr = Long.parseLong(lenSvr);
        fileSvr.lenLoc = Long.parseLong(lenLoc);
        fileSvr.sizeLoc = PathTool.BytesToString(fileSvr.lenLoc);
        fileSvr.pathLoc = pathLoc;
        fileSvr.pathSvr = pathSvr;
        fileSvr.pathRel=pathRel;
        fileSvr.perSvr="100%";
        fileSvr.complete=StringUtils.equalsIgnoreCase(complete,"true");
        fileSvr.blockIndex=Integer.parseInt(blockIndex);
        fileSvr.blockOffset = Long.parseLong(blockOffset);
        fileSvr.blockSize = blockSize;//加密后的块大小
        fileSvr.blockCount = Integer.parseInt(blockCount);
        fileSvr.nameLoc = PathTool.getName(pathLoc);
        fileSvr.nameSvr = fileSvr.nameLoc;
        fileSvr.encrypt = ConfigReader.storageEncrypt();//存储加密

        //块加密
        if (blockEncrypt)
        {
            //大小不相同
            if(blockSizeCry!=ostm.size())
            {
                JSONObject o = new JSONObject();
                o.put("blockSizeCry",blockSizeCry);
                o.put("blockSizeRecv",ostm.size());
                return this.send_error("blockSizeCryDifferent",o);
            }
            CryptoTool ct   = new CryptoTool();
            fileSvr.pathSvr = ct.decrypt(pathSvr);
            if(!fileSvr.encrypt) ostm = ct.decrypt(ostm,blockSize);
        }//块大小不同
        else if(blockSize!=ostm.size())
        {
            JSONObject o = new JSONObject();
            o.put("blockSizeSend",blockSize);
            o.put("blockSizeRecv",ostm.size());
            return this.send_error("blockSizeDifferent",o);
        }

        //token验证
        WebSafe ws = new WebSafe();
        if(!ws.validToken(token,fileSvr,"block"))
        {
            return this.send_error("tokenValidFail",JSONObject.fromObject(fileSvr));
        }

        //验证文件块MD5
        String md5Svr = "";
        if(!StringUtils.isBlank(blockMd5))
        {
            md5Svr = Md5Tool.fileToMD5(ostm);
            if(md5Svr.equals(blockMd5))
            {
                JSONObject o = new JSONObject();
                o.put("md5Svr",md5Svr);
                o.put("md5Loc",blockMd5);
                return this.send_error("blockSizeDifferent",o);
            }
        }

        PathBuilder pb = new PathBuilder();
        try {
            fileSvr.pathSvr = pb.relToAbs(fileSvr.pathSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //选择写入器
        FileBlockWriter fw = ConfigReader.blockWriter();
        boolean needGenId = !pid.isEmpty();//子文件需要创建
        if(needGenId) needGenId = 1 == fileSvr.blockIndex;

        //子文件.仅第一块生成存储.id
        try {
            if(needGenId) {
                fileSvr.object_id = fw.make(fileSvr);
                fileSvr.object_key = fileSvr.getObjectKey();
            }

            //保存层级信息
            fileSvr.saveScheme();
            //保存文件块数据
            fw.write(fileSvr,ostm);
            //合并文件
            if(isLastBlock) fw.writeLastPart(fileSvr);
        }
        catch (IOException e)
        {
            return this.send_error("writeBlockFail",JSONObject.fromObject(fileSvr));
        }

        up6_biz_event.file_post_block(id,Integer.parseInt(blockIndex));

        JSONObject o = new JSONObject();
        o.put("msg", "ok");
        o.put("md5", md5Svr);
        o.put("offset", blockOffset);//基于文件的块偏移位置
        //将FastDFS.id回传给控件
        JSONObject fds = new JSONObject();
        fds.put("object_id",fileSvr.object_id);//将UploadId回传给控件
        if(StringUtils.equals(blockIndex,"1")) o.put("fields",fds);
        return o.toString();
    }

    /**
     * 更新文件进度，由前端调用。
     * 调用位置：up6.file.js
     * @param id 文件ID
     * @param uid 用户ID
     * @param offset 偏移位置
     * @param lenSvr 已传大小
     * @param perSvr 已传百分比
     * @param cbk JQ回调方法
     * @return
     */
    @RequestMapping(value="up6/f_process",method = RequestMethod.GET)
    public String f_process(
                        @RequestParam(value="id", required=false,defaultValue="")String id,
                        @RequestParam(value="uid", required=false,defaultValue="")String uid,
                        @RequestParam(value="offset", required=false,defaultValue="")String offset,
                        @RequestParam(value="lenSvr", required=false,defaultValue="")String lenSvr,
                        @RequestParam(value="perSvr", required=false,defaultValue="")String perSvr,
                        @RequestParam(value="callback", required=false,defaultValue="")String cbk
    ) throws ParseException, IllegalAccessException, SQLException {
        int ret = 0;
        if (	!StringUtils.isBlank(id)
                &&	!StringUtils.isBlank(lenSvr)
                &&	!StringUtils.isBlank(perSvr))
        {
            DBConfig cfg = new DBConfig();
            DBFile db = cfg.db();
            db.f_process(Integer.parseInt(uid),id,Long.parseLong(offset),Long.parseLong(lenSvr),perSvr);
            up6_biz_event.file_post_process(id);
            ret = 1;
        }
        return cbk + "({\"value\":"+ret+"})";
    }

    @RequestMapping(value="up6/nosql/f_process",method = RequestMethod.GET)
    public String f_process_nosql(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="")String uid,
            @RequestParam(value="offset", required=false,defaultValue="")String offset,
            @RequestParam(value="lenSvr", required=false,defaultValue="")String lenSvr,
            @RequestParam(value="perSvr", required=false,defaultValue="")String perSvr,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    )
    {
        int ret = 0;
        if (	!StringUtils.isBlank(id)
                &&	!StringUtils.isBlank(lenSvr)
                &&	!StringUtils.isBlank(perSvr))
        {
            up6_biz_event.file_post_process(id);
            ret = 1;
        }
        return cbk + "({\"value\":"+ret+"})";
    }

    /**
     * 文件夹上传完毕，在文件夹上传完毕后自动调用。由前端调用
     * 调用位置：up6.folder.js
     * @param id 文件ID
     * @param uid 用户ID
     * @param cbk JQ回调方法
     * @return
     */
    @RequestMapping(value="up6/fd_complete",method = RequestMethod.GET)
    public String fd_complete(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="0")Integer uid,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    ) throws IOException, SQLException, ParseException, IllegalAccessException {
        int ret = 0;

        //参数为空
        if (!StringUtils.isBlank(id))
        {
            //取当前节点信息
            FileInf folder = DBFile.build().read(id);
            folder.uid = uid;

            //根节点
            FileInf root = new FileInf();
            root.id = folder.pidRoot;
            root.uid = folder.uid;
            //当前节点是根节点
            if( StringUtils.isBlank(root.id)) root.id = folder.id;

            //从文件中解析层级结构信息
            FolderSchemaDB fsd = new FolderSchemaDB();
            fsd.save(folder);

            //上传完毕
            DBFile.build().complete(id,uid);

            up6_biz_event.folder_post_complete(id);
            ret = 1;
        }
        return cbk + "(" + ret + ")";
    }

    @RequestMapping(value="up6/nosql/fd_complete",method = RequestMethod.GET)
    public String fd_complete_nosql(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="")String uid,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    ) throws IOException, SQLException {
        int ret = 0;

        //参数为空
        if (	!StringUtils.isBlank(uid)||
                !StringUtils.isBlank(id))
        {
            up6_biz_event.folder_post_complete(id);

            ret = 1;
        }
        return cbk + "(" + ret + ")";
    }

    /**
     * 文件夹初始化，在上传文件夹时自动调用。
     * 调用位置：up6.folder.js
     * @param id 文件夹ID，GUID格式
     * @param uid 用户ID
     * @param lenLoc 数字化的文件夹大小：1024
     * @param sizeLoc 格式化的文件夹大小：10MB
     * @param pathLoc 文件夹本地路径。
     * @param cbk JQ回调方法，提供跨域调用
     * @return
     */
    @RequestMapping(value="up6/fd_create",method = RequestMethod.GET)
    public String fd_create(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="")String uid,
            @RequestParam(value="lenLoc", required=false,defaultValue="")String lenLoc,
            @RequestParam(value="sizeLoc", required=false,defaultValue="")String sizeLoc,
            @RequestParam(value="pathLoc", required=false,defaultValue="")String pathLoc,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    ) throws ParseException, IllegalAccessException, SQLException {
        //参数为空
        if (
                StringUtils.isBlank(id)||
                StringUtils.isBlank(uid)||
                StringUtils.isBlank(pathLoc))
        {
            return (cbk + "({\"value\":null})");
        }
        pathLoc = PathTool.url_decode(pathLoc);


        FileInf fileSvr = new FileInf();
        fileSvr.id      = id;
        fileSvr.fdChild = false;
        fileSvr.fdTask  = true;
        fileSvr.uid     = Integer.parseInt(uid);
        fileSvr.nameLoc = PathTool.getName(pathLoc);
        fileSvr.pathLoc = pathLoc;
        fileSvr.lenLoc  = Long.parseLong(lenLoc);
        fileSvr.sizeLoc = sizeLoc;
        fileSvr.deleted = false;
        fileSvr.nameSvr = fileSvr.nameLoc;

        //生成存储路径
        PathBuilderUuid pb = new PathBuilderUuid();
        try {
            fileSvr.pathSvr = pb.genFolder(fileSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");
        if(!PathTool.createDirectory(fileSvr.pathSvr))
        {
            res.setStatus(500);
            return "create dir error";
        }

        //创建层级结构
        FolderSchema fst = new FolderSchema();
        if(!fst.create(fileSvr))
        {
            res.setStatus(500);
            return "create dir schema error";
        }

        //添加到数据表
        DBConfig cfg = new DBConfig();
        DBFile db = cfg.db();
        db.Add(fileSvr);

        //传输加密
        if (ConfigReader.postEncrypt())
        {
            CryptoTool ct   = new CryptoTool();
            try {
                fileSvr.pathSvr = ct.encrypt(fileSvr.pathSvr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        up6_biz_event.folder_create(fileSvr);

        Gson g = new Gson();
        String json = g.toJson(fileSvr);
        try {
            json = URLEncoder.encode(json,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        json = json.replace("+","%20");

        JSONObject ret = new JSONObject();
        ret.put("value",json);
        json = cbk + String.format("(%s)",ret.toString());//返回jsonp格式数据。
        return json;
    }

    @RequestMapping(value="up6/nosql/fd_create",method = RequestMethod.GET)
    public String fd_create_nosql(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="")String uid,
            @RequestParam(value="lenLoc", required=false,defaultValue="")String lenLoc,
            @RequestParam(value="sizeLoc", required=false,defaultValue="")String sizeLoc,
            @RequestParam(value="pathLoc", required=false,defaultValue="")String pathLoc,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    )
    {
        //参数为空
        if (
                StringUtils.isBlank(id)||
                        StringUtils.isBlank(uid)||
                        StringUtils.isBlank(pathLoc))
        {
            return (cbk + "({\"value\":null})");
        }
        pathLoc = PathTool.url_decode(pathLoc);


        FileInf fileSvr = new FileInf();
        fileSvr.id      = id;
        fileSvr.fdChild = false;
        fileSvr.fdTask  = true;
        fileSvr.uid     = Integer.parseInt(uid);
        fileSvr.nameLoc = PathTool.getName(pathLoc);
        fileSvr.pathLoc = pathLoc;
        fileSvr.lenLoc  = Long.parseLong(lenLoc);
        fileSvr.sizeLoc = sizeLoc;
        fileSvr.deleted = false;
        fileSvr.nameSvr = fileSvr.nameLoc;

        //生成存储路径
        PathBuilderUuid pb = new PathBuilderUuid();
        try {
            fileSvr.pathSvr = pb.genFolder(fileSvr);
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileSvr.pathSvr = fileSvr.pathSvr.replace("\\","/");
        if(!PathTool.createDirectory(fileSvr.pathSvr))
        {
            res.setStatus(500);
            return "create dir error";
        }

        //创建层级结构
        FolderSchema fst = new FolderSchema();
        if(!fst.create(fileSvr))
        {
            res.setStatus(500);
            return "create dir schema error";
        }

        //传输加密
        if (ConfigReader.postEncrypt())
        {
            CryptoTool ct   = new CryptoTool();
            try {
                fileSvr.pathSvr = ct.encrypt(fileSvr.pathSvr);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        up6_biz_event.folder_create(fileSvr);

        Gson g = new Gson();
        String json = g.toJson(fileSvr);
        try {
            json = URLEncoder.encode(json,"utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        json = json.replace("+","%20");

        JSONObject ret = new JSONObject();
        ret.put("value",json);
        json = cbk + String.format("(%s)",ret.toString());//返回jsonp格式数据。
        return json;
    }

    /**
     * 删除文件夹，在文件列表中点击删除按钮时调用，由前端调用
     * 调用位置：up6.js
     * @param id 文件夹ID
     * @param uid 用户ID
     * @param cbk JQ回调方法，提供跨域调用
     * @return
     */
    @RequestMapping(value="up6/fd_del",method = RequestMethod.GET)
    public String fd_del(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="0")int uid,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    ) throws ParseException, IllegalAccessException, SQLException {
        int ret = 0;
        //参数为空
        if (	!StringUtils.isBlank(id))
        {
            DBFile.build().Delete(uid,id);
            ret = 1;
        }
        return cbk + "({\"value\":"+ret+"})";
    }

    @RequestMapping(value="up6/nosql/fd_del",method = RequestMethod.GET)
    public String fd_del_nosql(
            @RequestParam(value="id", required=false,defaultValue="")String id,
            @RequestParam(value="uid", required=false,defaultValue="")String uid,
            @RequestParam(value="callback", required=false,defaultValue="")String cbk
    )
    {
        int ret = 0;
        //参数为空
        if (	!StringUtils.isBlank(id)||
                uid.length()>0 )
        {
            ret = 1;
        }
        return cbk + "({\"value\":"+ret+"})";
    }

    /**
     * 发送错误信息
     * @param errCode
     * @param par
     */
    String send_error(String errCode,JSONObject par) {
        OutputStream os = null;
        JSONObject o = new JSONObject();
        o.put("msg",errCode);
        o.put("errCode",errCode);//错误码
        o.put("md5","");
        o.put("offset",-1);
        o.put("param",par);
        return o.toString();
    }
}
