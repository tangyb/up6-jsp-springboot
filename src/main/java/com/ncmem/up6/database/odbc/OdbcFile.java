package com.ncmem.up6.database.odbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.ncmem.up6.database.DBFile;
import com.ncmem.up6.database.DbHelper;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.sql.SqlExec;
import com.ncmem.up6.sql.SqlWhereMerge;
import net.sf.json.JSONArray;
import org.apache.commons.lang.StringUtils;

public class OdbcFile extends DBFile {

	public boolean existSameFile(String name,String pid)
	{
		SqlWhereMerge swm = new SqlWhereMerge();
		swm.equal("f_nameLoc", name.trim());
		swm.equal("f_deleted", 0);
		if(StringUtils.isBlank(pid)) pid=" ";
		swm.equal("nvl(f_pid,' ')", pid);
		String where = swm.to_sql();

		String sql = String.format("select f_id from up6_files where %s", where);

		SqlExec se = new SqlExec();
		JSONArray fid = se.exec("up6_files", sql, "f_id", "");
		return fid.size() > 0;
	}
}
