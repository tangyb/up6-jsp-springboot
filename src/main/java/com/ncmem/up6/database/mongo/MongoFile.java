package com.ncmem.up6.database.mongo;

import com.google.gson.Gson;
import com.ncmem.up6.database.DBFile;
import com.ncmem.up6.model.DnFileInf;
import com.ncmem.up6.SpringUtil;
import com.ncmem.up6.model.FileInf;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;

public class MongoFile extends DBFile
{
    private MongoTemplate mongo;

    public MongoFile(){
        this.mongo = SpringUtil.getBean(MongoTemplate.class);
    }

    public String unCompletes(int f_uid){

        Query query = new Query(Criteria.where("complete").is(true)
                .orOperator(Criteria.where("uid").is(f_uid)));
        List<DnFileInf> fs = this.mongo.find(query,DnFileInf.class,"up6_files");

        Gson g = new Gson();
        return g.toJson( fs );
    }

    public FileInf read(String f_id){
        return this.mongo.findById(f_id,FileInf.class);
    }

    public FileInf exist_file(String md5){

        Query q = new Query(Criteria.where("md5").is(md5));
        FileInf fs = this.mongo.findOne(q,FileInf.class);
        return fs;
    }

    public void Add(FileInf f){
        this.mongo.insert(f);
    }

    /**
     * 清空数据集
     */
    public void clear(){
        Query query = new Query(Criteria.where("complete").is(true)
                .orOperator(Criteria.where("complete").is(false)));
        List<FileInf> fs = this.mongo.findAll(FileInf.class);
        for(int i = 0 , l = fs.size() ; i < l ; ++i)
        {
            FileInf f = (FileInf)fs.get(i);
            this.mongo.remove(f);
        }
        System.out.println("文件数据集清空完毕");

        fs = this.mongo.findAll(FileInf.class, "up6_folders");
        for(int i = 0 , l = fs.size() ; i < l ; ++i)
        {
            FileInf f = (FileInf)fs.get(i);
            this.mongo.remove(f);
        }

        System.out.println("目录数据集清空完毕");
    }

    public boolean f_process(int uid,String f_id,long offset,long f_lenSvr,String f_perSvr)
    {
        Query q = new Query(Criteria.where("id").is(f_id));
        Update up = new Update();
        up.set("offset",offset)
                .set("lenSvr",f_lenSvr)
                .set("perSvr",f_perSvr);
        this.mongo.updateFirst(q,up,FileInf.class);

        return  true;
    }

    public void complete(String id,int uid)
    {
        Query q = new Query(Criteria.where("id").is(id)
                .orOperator(Criteria.where("uid").is(uid)));
        Update up = new Update();
        up.set("complete",true)
        .set("scaned",true)
        .set("perSvr","100");
        this.mongo.updateFirst(q, up, FileInf.class);

    }

    public void Delete(int f_uid,String f_id)
    {
        Query q = new Query(Criteria.where("id").is(f_id));
        this.mongo.remove(q,"down_files");
    }

    public void delete(String pathRel,int uid,String id)
    {
        Query q = new Query(Criteria.where("id").is(id)
                .orOperator(Criteria.where("uid").is(uid))
                .orOperator(Criteria.where("pathRel").is(pathRel)));
        this.mongo.remove(q);
    }
}
