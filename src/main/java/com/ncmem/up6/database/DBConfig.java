package com.ncmem.up6.database;

import com.ncmem.up6.database.mongo.MongoFile;
import com.ncmem.up6.database.mysql.MySQLFile;
import com.ncmem.up6.database.mysql.MySQLFolder;
import com.ncmem.up6.database.odbc.OdbcFile;
import com.ncmem.up6.database.odbc.OdbcFolder;
import com.ncmem.up6.database.oracle.OracleFile;
import com.ncmem.up6.database.oracle.OracleFolder;
import com.ncmem.up6.utils.ConfigReader;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by Administrator on 2021/1/6.
 */
public class DBConfig {
    public String m_db="sql";//sql,oracle,mysql

    String driver = "";
    String url = "";
    String name = "";
    String pass = "";
    public Boolean m_isOdbc=false;

    public DBConfig() {
        ConfigReader cr = new ConfigReader();
        this.m_db = cr.readString("$.database.connection.type");
        JSONObject o = cr.read("$.database.connection."+this.m_db);
        this.driver = o.getString("driver");
        this.url = o.getString("url");
        this.name = o.getString("name");
        this.pass = o.getString("pass");
        this.m_isOdbc = StringUtils.equals(this.m_db,"kingbase");
    }

    public DBFile db() {
        if( StringUtils.equals(this.m_db, "mysql") ) return new MySQLFile();
        else if( StringUtils.equals(this.m_db, "oracle")||
                StringUtils.equals(this.m_db, "dmdb"))
            return new OracleFile();
        else if( StringUtils.equals(this.m_db, "kingbase") ) return new OdbcFile();
        else if( StringUtils.equals(this.m_db, "mongodb") ) return new MongoFile();
        else return new DBFile();
    }

    public DbFolder folder()
    {
        if( StringUtils.equals(this.m_db, "sql") ) return new DbFolder();
        else if( StringUtils.equals(this.m_db, "mysql") ) return new MySQLFolder();
        else if( StringUtils.equals(this.m_db, "oracle")||
                StringUtils.equals(this.m_db, "dmdb"))
            return new OracleFolder();
        else if( StringUtils.equals(this.m_db, "kingbase") ) return new OdbcFolder();
        else if( StringUtils.equals(this.m_db, "mongodb") ) return new DbFolderMongodb();
        else return new DbFolder();
    }

    public Connection getCon()
    {
        Connection con = null;

        try
        {
            Class.forName(this.driver).newInstance();//加载驱动。
            if (StringUtils.equals(this.m_db, "mysql")) con = DriverManager.getConnection(this.url);
            else con = DriverManager.getConnection(this.url,this.name,this.pass);
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InstantiationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return con;
    }
}
