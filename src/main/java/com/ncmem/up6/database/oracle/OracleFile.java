package com.ncmem.up6.database.oracle;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.ncmem.up6.database.DBFile;
import com.ncmem.up6.database.DbHelper;
import net.sf.json.JSONArray;

import org.apache.commons.lang.StringUtils;

import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.sql.SqlExec;
import com.ncmem.up6.sql.SqlWhereMerge;

public class OracleFile extends DBFile
{
	public boolean existSameFile(String name,String pid)
	{
		SqlWhereMerge swm = new SqlWhereMerge();
        swm.equal("f_nameLoc", name.trim());
        swm.equal("f_deleted", 0);
        if(StringUtils.isBlank(pid)) pid=" ";
        swm.equal("nvl(f_pid,' ')", pid);
        String where = swm.to_sql();

        String sql = String.format("select f_id from up6_files where %s", where);

        SqlExec se = new SqlExec();
        JSONArray fid = se.exec("up6_files", sql, "f_id", "");
        return fid.size() > 0;		
	}
	
	/**
	 * 更新子文件路径
	 * @param pathRelOld
	 * @param pathRelNew
	 */
	public void updatePathRel(String pathRelOld,String pathRelNew)
	{
        //更新子文件路径
        String sql = String.format("update up6_files set f_pathRel=REPLACE(f_pathRel,'%s/','%s/') where instr(f_pathRel,'%s/')>0",
            pathRelOld,
            pathRelNew,
            pathRelOld
            );
        
        SqlExec se = new SqlExec();
        se.exec(sql);
		
	}
}
