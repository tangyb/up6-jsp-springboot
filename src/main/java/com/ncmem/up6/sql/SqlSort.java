package com.ncmem.up6.sql;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class SqlSort {

    /// <summary>
    /// id desc time desc
    /// </summary>
    List<String> items = new ArrayList<String>();

    public static SqlSort build() {
        return new SqlSort();
    }

    public SqlSort() { }

    public SqlSort desc(String field) {
        this.items.add(field+" desc");
        return this;
    }
    public SqlSort asc(String field) {
        this.items.add(field+" asc");
        return this;
    }

    public String toSql() {
        if (this.items.size() == 0) return "";
        return " order by " + StringUtils.join(this.items,',');
    }
}
