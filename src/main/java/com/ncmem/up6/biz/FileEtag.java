package com.ncmem.up6.biz;

import com.google.gson.Gson;
import com.ncmem.up6.utils.FileTool;
import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;

public class FileEtag {
    /**
     *
     * @param f
     */
    public void saveTags(FileInf f) {
        String file = f.ETagsFile();
        PathTool.mkdirsFromFile(file);
        //System.out.println("ETag.file="+file);

        Gson g = new Gson();
        String val = g.toJson(f);
        FileTool.appendLine(file, val);
    }
}
