package com.ncmem.up6.biz;

import java.io.IOException;

import com.ncmem.up6.utils.ConfigReader;
import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;


public class PathBuilder {
	
	public PathBuilder(){}
	
	/**
	 * 获取上传路径
	 * 格式：
	 * 	resources/upload
	 * @return
	 * @throws IOException
	 */
	public String getRoot() throws IOException{
		//F:/jsp/springboot/up6/target/classes/
		String root = this.getClass().getResource("/").getPath();
		int pos = root.indexOf("target/");
		if(-1!=pos) root = root.substring(1,pos).replace("//","/");


		ConfigReader cr = new ConfigReader();
		String pathSvr = cr.m_jp.read("IO.dir");
		pathSvr = pathSvr.trim();//
		pathSvr = pathSvr.replace("{root}", root);
		pathSvr = pathSvr.replaceAll("\\\\", "/");
		return pathSvr;
	}
	public String genFolder(FileInf fd) throws IOException{return "";}
	public String genFile(int uid,FileInf f) throws IOException{return "";}
	public String genFile(int uid,String md5,String nameLoc)throws IOException{return "";}
	/**
	 * 相对路径转换成绝对路径
	 * 格式：
	 * 	/2021/05/28/guid/nameLoc => d:/upload/2021/05/28/guid/nameLoc
	 * @return
	 * @throws IOException
	 */
	public String relToAbs(String path) throws IOException
	{
		String root = this.getRoot();
		root = root.replaceAll("\\\\", "/");
		path = path.replaceAll("\\\\", "/");
		if(path.startsWith("/"))
		{
			path = PathTool.combine(root, path);
		}
		return path.trim();
	}
	/**
	 * 将路径转换成相对路径
	 * @return
	 * @throws IOException
	 */
	public String absToRel(String path) throws IOException
	{
		String root = this.getRoot().replaceAll("\\\\", "/");
		path = path.replaceAll("\\\\", "/");
		path = path.replaceAll(root, "");
		return path;
	}
}
