package com.ncmem.up6.biz;

import com.google.gson.Gson;
import com.ncmem.up6.utils.FileTool;
import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;

import java.io.File;

public class FolderSchema {

    public FolderSchema() {}

    /**
     * 创建目录信息
     * pathSvr/folder.txt
     * 结构：
     * folder,id,pid,pidRoot,pathSvr,pathRel,lenLoc
     * @param dir
     */
    public boolean create(FileInf dir)
    {
        String file = dir.schemaFile();
        Gson g = new Gson();
        String val = g.toJson(dir);
        return FileTool.writeAll(file, val+"\n");
    }

    public void addFile(FileInf f) {
        String root = f.pathSvr.replace(f.pathRel, "");
        root = PathTool.getParent(root);
        String file = root + "/schema.txt";
        Gson g = new Gson();
        String val = g.toJson(f);

        //结构文件不存在
        File fe = new File(file);
        if(!fe.exists()) return;

        FileTool.appendLine(file, val);
    }
}
