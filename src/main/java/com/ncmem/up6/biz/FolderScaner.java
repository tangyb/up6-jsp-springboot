package com.ncmem.up6.biz;

import com.ncmem.up6.database.DBFile;
import com.ncmem.up6.database.DbFolder;
import com.ncmem.up6.model.FileInf;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;

public class FolderScaner extends FolderSchemaDB
{
    public Boolean m_cover=false;
    public void save(FileInf dir) throws IOException, ParseException, IllegalAccessException, SQLException {
        this.m_root=dir;
        this.m_files = new ArrayList<FileInf>();
        this.m_folders = new ArrayList<FileInf>();
        this.m_dirs = new HashMap<String,FileInf>();

        //加载层级信息
        this.loadFiles(dir);

        //分析
        this.parseParent();
        this.parseDirs();
        this.updatePID();
        this.updatePathRel();

        //取消根目录
        this.m_folders.remove(0);
        //覆盖同名项
        if(this.m_cover) this.cover();

        DbFolder.build().addBatch(this.m_folders);
        DBFile.build().addBatch(this.m_files);
    }
}
