package com.ncmem.up6.utils;

import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.ReadContext;

import com.ncmem.up6.store.*;
import com.ncmem.up6.store.aliyun.OSSReader;
import com.ncmem.up6.store.aliyun.OSSWriter;
import com.ncmem.up6.store.cos.CosReader;
import com.ncmem.up6.store.cos.CosWriter;
import com.ncmem.up6.store.fastdfs.FastDFSReader;
import com.ncmem.up6.store.fastdfs.FastDFSWriter;
import com.ncmem.up6.store.minio.MinioReader;
import com.ncmem.up6.store.minio.MinioWriter;
import com.ncmem.up6.store.obs.ObsReader;
import com.ncmem.up6.store.obs.ObsWriter;
import net.sf.json.JSONObject;

import java.io.IOException;

/**
 * Created by jmzy on 2021/1/5.
 */
public class ConfigReader {
    public JSONObject m_files;
    public PathTool m_pt;
    public ReadContext m_jp;

    public ConfigReader() {
        this.m_pt = new PathTool();

        try {
            String json = ResTool.readFile("config/config.json");
            this.m_files = JSONObject.fromObject(json);
            this.m_jp = JsonPath.parse(json);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static DataBaseType dbType(){
        ConfigReader r = new ConfigReader();
        String t = r.readString("database.connection.type");

        if(t.compareToIgnoreCase("mysql") ==0) return DataBaseType.MySQL;
        else if(t.compareToIgnoreCase("oracle") ==0) return DataBaseType.Oracle;
        else if(t.compareToIgnoreCase("sql") ==0) return DataBaseType.SqlServer;
        else if(t.compareToIgnoreCase("dmdb") ==0) return DataBaseType.DMDB;
        else if(t.compareToIgnoreCase("kingbase") ==0) return DataBaseType.KingBase;
        else if(t.compareToIgnoreCase("mongodb") ==0) return DataBaseType.MongoDB;
        return DataBaseType.SqlServer;
    }

    /**
     * 将配置加载成一个json对象
     * @param name
     * @return
     */
    public JSONObject module(String name)
    {
        String path = this.m_jp.read(name);
        String data = "";
        try {
            data = ResTool.readFile(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return JSONObject.fromObject(data);
    }

    /**
     * 将JSON值转换成一个路径
     * @param name
     * @return
     */
    public String readPath(String name)
    {
        Object o = this.m_jp.read(name);
        String v = o.toString();
        v = this.m_pt.MapPath(v);
        return v;
    }

    public JSONObject read(String name)
    {
        Object o = this.m_jp.read(name);
        return JSONObject.fromObject(o);
    }

    /**
     * 以jsonpath方式读取config.json中的string值
     * @param name
     * @return
     */
    public String readString(String name)
    {
        Object o = this.m_jp.read(name);
        return o.toString();
    }

    /**
     * 以jsonpath方式读取config.json中的bool值
     * @param name
     * @return
     */
    public boolean readBool(String name)
    {
        Object o = this.m_jp.read(name);
        return Boolean.parseBoolean(o.toString());
    }

    /**
     * 获取存储类型
     * @return
     */
    public static StorageType storageType(){
        ConfigReader cr = new ConfigReader();
        String sto = cr.readString("Storage.type");
        if(sto.compareToIgnoreCase("FastDFS") ==0) return StorageType.FastDFS;
        else if(sto.compareToIgnoreCase("Minio") ==0) return StorageType.Minio;
        else if(sto.compareToIgnoreCase("OBS") ==0) return StorageType.OBS;
        else if(sto.compareToIgnoreCase("OSS") ==0) return StorageType.OSS;
        else if(sto.compareToIgnoreCase("COS") ==0) return StorageType.COS;
        return StorageType.IO;
    }

    /**
     * 传输加密
     * @return
     */
    public static boolean postEncrypt() {
        ConfigReader cr = new ConfigReader();
        JSONObject security = cr.m_files.getJSONObject("security");
        boolean encrypt = security.getBoolean("encrypt");
        return encrypt;
    }

    /**
     * 存储加密
     * @return
     */
    public static boolean storageEncrypt(){
        ConfigReader cr = new ConfigReader();
        return cr.readBool("Storage.encrypt");
    }

    /**
     * 块数据写入器
     * @return
     */
    public static FileBlockWriter blockWriter(){
        FileBlockWriter w = null;
        StorageType st = ConfigReader.storageType();
        if(st==StorageType.FastDFS) w = new FastDFSWriter();
        else if(st==StorageType.Minio) w = new MinioWriter();
        else if(st==StorageType.OBS) w = new ObsWriter();
        else if(st==StorageType.OSS) w = new OSSWriter();
        else if(st==StorageType.COS) w = new CosWriter();
        else  w = new FileBlockWriter();
        return w;
    }

    /**
     * 块数据读取器
     * @return
     */
    public static FileBlockReader blockReader(){
        FileBlockReader w = null;
        StorageType st = ConfigReader.storageType();
        if(st==StorageType.FastDFS) w = new FastDFSReader();
        else if(st==StorageType.Minio) w = new MinioReader();
        else if(st==StorageType.OBS) w = new ObsReader();
        else if(st==StorageType.OSS) w = new OSSReader();
        else if(st==StorageType.COS) w = new CosReader();
        else  w = new FileBlockReader();
        return w;
    }
}
