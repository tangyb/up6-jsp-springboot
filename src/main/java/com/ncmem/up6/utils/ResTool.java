package com.ncmem.up6.utils;

import org.springframework.core.io.ClassPathResource;

import java.io.*;

public class ResTool {

    /**
     * 加载文件内容
     * @param path
     * @return
     * @throws IOException
     */
    public static String readFile(String path) throws IOException
    {
        ClassPathResource res = new ClassPathResource(path);
        Object ins = (Object)res.getInputStream();
        BufferedReader br = null;
        if (ins == null) return "";

        if (ins instanceof String)
        {
            br = new BufferedReader(new FileReader(new File((String) ins)));
        } else if (ins instanceof InputStream)
        {
            br = new BufferedReader(new InputStreamReader((InputStream) ins));
        }
        String str="";
        String line;
        while ((line = br.readLine()) != null)
        {
            str += line;
        }
        br.close();
        return str;
    }

}
