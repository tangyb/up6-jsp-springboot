package com.ncmem.up6.utils;

import org.apache.tomcat.util.http.fileupload.FileItem;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class StreamTool {

    public static ByteArrayOutputStream toStream(MultipartFile b)
    {
        ByteArrayOutputStream s =null;
        try {
            InputStream stream = b.getInputStream();
            byte[] buf = new byte[(int)b.getSize()];
            stream.read(buf);
            stream.close();

            s = new ByteArrayOutputStream();
            s.write(buf);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return s;
    }
}
