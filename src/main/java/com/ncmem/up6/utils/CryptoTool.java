package com.ncmem.up6.utils;
import java.io.ByteArrayOutputStream;
import java.nio.charset.StandardCharsets;
import java.security.Security;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import sun.misc.BASE64Decoder;
import com.ncmem.up6.model.FileInf;

/**
 * 
 * @author zysoft
 * 用法：
 *
 */
public class CryptoTool
{
	private String key = "2C4DD1CC9KAX4TA9";
	private String iv = "2C4DD1CC9KAX4TA9";
	 //算法名称
	private String KEY_ALGORITHM = "AES";
	 //加密算法，填充方式
	 private String algorithm = "AES/CBC/NoPadding";

	
	public CryptoTool()
	{		
	}	
	 
	public String encrypt(String data) throws Exception 
	{
	    try 
	    {
	    	byte[] buf = data.getBytes("UTF-8");
	    	if((buf.length%16) >0)
			{
				int len = 16 - buf.length%16;
				buf = Arrays.copyOf(buf,buf.length+len);
			}

	    	Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
	        Cipher cipher = Cipher.getInstance(this.algorithm);

	        SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), this.KEY_ALGORITHM);
	        IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
	
	        cipher.init(Cipher.ENCRYPT_MODE, keyspec, ivspec);
	        byte[] cry = cipher.doFinal(buf);
	        String str = new sun.misc.BASE64Encoder().encode(cry);
	        return str.trim();
	
	    } catch (Exception e) {
	        e.printStackTrace();
	        return null;
	    }
	}

    public String decrypt(String data)
    {
        try
        {
            byte[] cry = new BASE64Decoder().decodeBuffer(data);
            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance(this.algorithm);
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), this.KEY_ALGORITHM);
            IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());
            cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);
            byte[] decry = cipher.doFinal(cry);

            String str = new String(decry,"UTF-8");
            return str.trim();
        }
        catch (Exception e) 
        {
            e.printStackTrace();
            return null;
        }
    }

	/**
	 *
	 * @param block
	 * @param lenOri 原始块大小
	 * @return
	 * @throws Exception
	 */
	public ByteArrayOutputStream decrypt(ByteArrayOutputStream block,int lenOri)
	{
		try
		{
			//16字节对齐
			int len = block.size()+16-block.size()%16;
			byte[] buf = Arrays.copyOf(block.toByteArray(),len);

			Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
			Cipher cipher = Cipher.getInstance(this.algorithm,"BC");
			SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), this.KEY_ALGORITHM);
			IvParameterSpec ivspec = new IvParameterSpec(iv.getBytes());

			cipher.init(Cipher.DECRYPT_MODE, keyspec, ivspec);

			byte[] out = cipher.doFinal(buf);
			ByteArrayOutputStream ost = new ByteArrayOutputStream();
			ost.write(out,0,lenOri);
			return ost;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public String token(FileInf f,String action)
	{
		String str = f.id + f.nameLoc + action;
		if(action == "block") str = f.id + f.pathSvr + action;
		str = Md5Tool.getMD5(str);
		try {
			str = this.encrypt(str);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
}