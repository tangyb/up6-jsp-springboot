package com.ncmem.up6.utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

public class FileTool {
	public static boolean writeAll(String pathSvr,String data)
	{
		try {
			FileOutputStream fos = new FileOutputStream(new File(pathSvr),false);
			OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(data);
			bw.close();
			osw.close();
			fos.close();
			return true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	public static void writeAll(String pathSvr,byte[] data)
	{
		try {
			FileOutputStream fos = new FileOutputStream(new File(pathSvr),false);
			BufferedOutputStream bw = new BufferedOutputStream(fos);
			bw.write(data);
			bw.close();
			fos.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void appendLine(String pathSvr,String data)
	{
		try {
			FileOutputStream fos = new FileOutputStream(new File(pathSvr),true);
			FileChannel fc = fos.getChannel();
			FileLock fl = null;
			//多线程写保护
			while(true)
			{
				try{
					fl = fc.tryLock();
					break;
				}
				catch(Exception e)
				{
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
			fl.release();
			OutputStreamWriter osw = new OutputStreamWriter(fos,"UTF-8");
			BufferedWriter bw = new BufferedWriter(osw);
			bw.write(data + "\n");
			bw.close();
			osw.close();
			fos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static char[] readAllChars(String path)
	{
		File f = new File(path);
		char[] data = new char[(int) f.length()];
		try {
			Reader r = new InputStreamReader(new FileInputStream(f));
			r.read(data);
			r.close();
			return data;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] readAllBytes(String path)
	{
		try {
			RandomAccessFile r = new RandomAccessFile(path,"r");
			long len = r.length();
			r.seek(0);
			byte[] data = new byte[(int)len];
			r.read(data);
			r.close();
			return data;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static String readAll(String path)
	{
		File f = new File(path);
		char[] data = new char[(int)f.length()];
		BufferedReader br;
		try {
			br = new BufferedReader( new InputStreamReader(new FileInputStream(f),"UTF-8"));
			br.read(data);
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String(data);		
	}
	public static String readAll(File f)
	{
		char[] data = new char[(int)f.length()];
		BufferedReader br;
		try {
			br = new BufferedReader( new InputStreamReader(new FileInputStream(f),"UTF-8"));
			br.read(data);
			br.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new String(data);
	}
}