package com.ncmem.up6.store.minio;

import com.ncmem.up6.utils.ConfigReader;

public class MinioConfig {
    public String ak="minioadmin";
    public String sk="minioadmin";
    public String service="s3";
    public String algorithm="AWS4-HMAC-SHA256";
    public String region="us-east-1";
    public String endpoint="192.168.0.111:9000";
    public String bucket="test";

    public MinioConfig () {
        ConfigReader cr = new ConfigReader();
        this.ak = cr.readString("Minio.ak");
        this.sk = cr.readString("Minio.sk");
        this.region = cr.readString("Minio.region");
        this.endpoint = cr.readString("Minio.endpoint");
        this.bucket = cr.readString("Minio.bucket");
    }
}
