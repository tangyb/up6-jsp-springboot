package com.ncmem.up6.store.minio;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.store.FileBlockReader;
import com.ncmem.up6.store.StorageType;

import java.io.IOException;

public class MinioReader extends FileBlockReader {
    public MinioReader() {
        this.storage = StorageType.Minio;
    }

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size) throws IOException {
        //还原成汉字，由okhttp自动转码。
        pathSvr = PathTool.url_decode(pathSvr);
        return MinioTool.getObject(pathSvr,  offset, size);
    }
}