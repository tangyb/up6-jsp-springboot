package com.ncmem.up6.store.cos;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.store.FileBlockReader;
import com.ncmem.up6.store.StorageType;

import java.io.IOException;

public class CosReader extends FileBlockReader {
    public CosReader() {
        this.storage = StorageType.COS;
    }

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size) throws IOException {
        //还原成汉字，由okhttp自动转码。
        pathSvr = PathTool.url_decode(pathSvr);
        return CosTool.getObject(pathSvr,  offset, size);
    }
}