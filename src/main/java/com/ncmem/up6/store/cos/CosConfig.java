package com.ncmem.up6.store.cos;

import com.ncmem.up6.utils.ConfigReader;

public class CosConfig {
    public String ak="minioadmin";
    public String sk="minioadmin";
    public String endpoint="192.168.0.111:9000";
    public String bucket="test";
    public String region="COS_REGION";

    public CosConfig () {
        ConfigReader cr = new ConfigReader();
        this.ak = cr.readString("COS.ak");
        this.sk = cr.readString("COS.sk");
        this.endpoint = cr.readString("COS.endpoint");
        this.bucket = cr.readString("COS.bucket");
        this.region = cr.readString("COS.region");
    }
}
