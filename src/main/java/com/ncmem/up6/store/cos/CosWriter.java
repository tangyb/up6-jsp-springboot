package com.ncmem.up6.store.cos;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.store.FileBlockWriter;
import com.ncmem.up6.store.StorageType;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CosWriter extends FileBlockWriter {

    public CosWriter() {
        this.storage= StorageType.COS;
    }

    /**
     * 生成UploadId
     */
    public String make(FileInf file) throws IOException {
        String ext = PathTool.getExtention(file.pathSvr);
        byte[] buf = new byte[0];
        String fileID = CosTool.CreateMultipartUpload(file.ObsKey());
        if(StringUtils.isEmpty(fileID)) throw new IOException("cos CreateMultipartUpload error");
        return fileID;
    }

    public String write(FileInf file, ByteArrayOutputStream ostm) throws IOException {
        byte[] data = ostm.toByteArray();
        file.etag = CosTool.UploadPart(file.ObsKey(), file.blockIndex, file.object_id, data);
        file.saveEtags();
        if(StringUtils.isEmpty(file.etag)) throw new IOException("cos upload part error");
        return file.etag;
    }

    /**
     * 写入最后一块数据=>所有文件块上传完毕
     * @param file
     * @return
     */
    public boolean writeLastPart(FileInf file) throws IOException {
        if( CosTool.CompleteMultipartUpload(file))return true;
        else throw new IOException("cos 合并文件错误");
    }
}