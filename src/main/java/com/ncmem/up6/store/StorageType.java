package com.ncmem.up6.store;

public enum StorageType {
    IO,FastDFS,Minio,OBS,OSS,COS
}