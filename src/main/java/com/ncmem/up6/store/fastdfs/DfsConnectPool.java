package com.ncmem.up6.store.fastdfs;

import com.ncmem.up6.utils.ConfigReader;
import com.ncmem.up6.XDebug;
import org.csource.common.MyException;
import org.csource.fastdfs.*;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * FastDFS连接池
 */
public class DfsConnectPool {

    //最大连接数,可以写配置文件
    private int size = 5;
    //被使用的连接
    private ConcurrentHashMap<StorageClient1,Object> busyConnectionPool = null;
    //空闲的连接
    private ArrayBlockingQueue<StorageClient1> idleConnectionPool = null;

    private Object obj = new Object();

    private static DfsConnectPool instance = new DfsConnectPool();

    public static DfsConnectPool getPool(){
        return instance;
    }

    //取出连接
    public StorageClient1 checkout(int waitTime){
        StorageClient1 c = null;
        try {
            c = idleConnectionPool.poll(waitTime, TimeUnit.SECONDS);
            System.out.println(c);
            if(c != null){
                busyConnectionPool.put(c, obj);
            }
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            c = null;
            System.out.println("DfsConnectPool.checkout.error");
            e.printStackTrace();
        }
        return c;
    }

    //回收连接
    public void checkin(StorageClient1 c){
        if(busyConnectionPool.remove(c) != null){
            idleConnectionPool.add(c);
            System.out.print("DfsConnectPool.checkin,回收连接,空闲连接：");
            System.out.println(idleConnectionPool.size());
        }
    }

    //如果连接无效则抛弃，新建连接来补充到池里
    public void drop(StorageClient1 storageClient1){
        if(busyConnectionPool.remove(storageClient1) != null){

            try {
                TrackerClient tracker = new TrackerClient();
                TrackerServer svr = tracker.getConnection();

                StorageServer svrSt = null;
                StorageClient1 client = new StorageClient1(svr,svrSt);

                idleConnectionPool.add(client);
                System.out.println("------------------------- :connection +1");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //单例
    private DfsConnectPool(){
        busyConnectionPool = new ConcurrentHashMap<StorageClient1, Object>();
        idleConnectionPool = new ArrayBlockingQueue<StorageClient1>(size);
        init(size);
    }

    //初始化连接池
    private void init(int size){
        initClientGlobal();
        TrackerServer svr = null;
        try {
            TrackerClient tracker = new TrackerClient();
            svr = tracker.getConnection();
            StorageServer storageServer = null;
            StorageClient1 client = null;
            for(int i=0; i<size; i++){
                client = new StorageClient1(svr,storageServer);
                idleConnectionPool.add(client);
                System.out.println("------------------------- :connection +1");
            }

        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            if(svr != null){
                try {
                    svr.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    //初始化客户端
    private void initClientGlobal(){
        ConfigReader cr = new ConfigReader();
        try {
            String ps = cr.readPath("FastDFS");
            ClientGlobal.init(ps);
            XDebug.Output("初始化FastDFS配置");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        }
    }
}
