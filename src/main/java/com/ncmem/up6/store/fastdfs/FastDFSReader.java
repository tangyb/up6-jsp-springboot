package com.ncmem.up6.store.fastdfs;

import com.ncmem.up6.store.FileBlockReader;
import com.ncmem.up6.store.StorageType;

public class FastDFSReader extends FileBlockReader {

    public FastDFSReader() {
        this.storage = StorageType.FastDFS;
     }

     /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size)
    {
        return FastDFSTool.down(pathSvr, offset, size);
    }
}
