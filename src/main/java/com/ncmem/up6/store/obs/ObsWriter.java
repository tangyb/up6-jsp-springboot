package com.ncmem.up6.store.obs;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.store.FileBlockWriter;
import com.ncmem.up6.store.StorageType;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class ObsWriter extends FileBlockWriter {

    public ObsWriter() {
        this.storage= StorageType.OBS;

    }

    /**
     * 生成UploadId
     */
    public String make(FileInf file) throws IOException {
        String ext = PathTool.getExtention(file.pathSvr);
        byte[] buf = new byte[0];
        String fileID = ObsTool.CreateMultipartUpload(file.ObsKey());
        if(StringUtils.isEmpty(fileID)) throw new IOException("obs InitiateMultipartUploadRequest error");
        return  fileID;
    }

    public String write(FileInf file, ByteArrayOutputStream ostm) throws IOException {
        byte[] data = ostm.toByteArray();
        file.etag = ObsTool.UploadPart(file.ObsKey(), file, data);
        file.saveEtags();
        if(StringUtils.isEmpty(file.etag)) throw new IOException("obs upload part error");
        return file.etag;
    }


    /**
     * 写入最后一块数据=>所有文件块上传完毕
     * @param file
     * @return
     */
    public boolean writeLastPart(FileInf file) throws IOException {
        boolean v = ObsTool.CompleteMultipartUpload(file);
        if(!v) throw  new IOException("obs CompleteMultipartUpload error");
        return  v;
    }
}
