package com.ncmem.up6.store.obs;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.store.FileBlockReader;
import com.ncmem.up6.store.StorageType;

import java.io.IOException;

public class ObsReader extends FileBlockReader {

    public ObsReader() {
        this.storage = StorageType.OBS;
    }

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size) throws IOException {
        //还原成汉字，由okhttp自动转码。
        pathSvr = PathTool.url_decode(pathSvr);
        return ObsTool.getObject(pathSvr,  offset, size);
    }
}
