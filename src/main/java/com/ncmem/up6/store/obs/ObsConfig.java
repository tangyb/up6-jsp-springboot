package com.ncmem.up6.store.obs;

import com.ncmem.up6.utils.ConfigReader;

public class ObsConfig {
    public String ak="minioadmin";
    public String sk="minioadmin";
    public String service="s3";
    public String algorithm="AWS4-HMAC-SHA256";
    public String region="us-east-1";
    public String endpoint="192.168.0.111:9000";
    public String bucket="test";

    public ObsConfig () {
        ConfigReader cr = new ConfigReader();
        this.ak = cr.readString("OBS.ak");
        this.sk = cr.readString("OBS.sk");
        this.region = cr.readString("OBS.region");
        this.endpoint = cr.readString("OBS.endpoint");
        this.bucket = cr.readString("OBS.bucket");
    }
}
