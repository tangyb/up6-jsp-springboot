package com.ncmem.up6.store.aliyun;

import com.ncmem.up6.utils.PathTool;
import com.ncmem.up6.model.FileInf;
import com.ncmem.up6.store.FileBlockWriter;
import com.ncmem.up6.store.StorageType;
import org.apache.commons.lang.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class OSSWriter extends FileBlockWriter {

    public OSSWriter() {
        this.storage= StorageType.OSS;
    }

    /**
     * 生成UploadId
     */
    public String make(FileInf file) throws IOException {
        String ext = PathTool.getExtention(file.pathSvr);
        byte[] buf = new byte[0];
        String fileID = OSSTool.CreateMultipartUpload(file.ObsKey());
        if(StringUtils.isEmpty(fileID)) throw new IOException("oss CreateMultipartUpload error");
        return fileID;
    }

    public String write(FileInf file, ByteArrayOutputStream ostm) throws IOException {
        byte[] data = ostm.toByteArray();
        file.etag = OSSTool.UploadPart(file.ObsKey(), file.blockIndex, file.object_id, data);
        file.saveEtags();
        if(StringUtils.isEmpty(file.etag)) throw new IOException("oss upload part error");
        return file.etag;
    }

    /**
     * 写入最后一块数据=>所有文件块上传完毕
     * @param file
     * @return
     */
    public boolean writeLastPart(FileInf file) throws IOException {        
        if( OSSTool.CompleteMultipartUpload(file))return true;
        else throw new IOException("oss 合并文件错误");
    }
}
