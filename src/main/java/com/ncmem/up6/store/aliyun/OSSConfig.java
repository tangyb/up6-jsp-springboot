package com.ncmem.up6.store.aliyun;

import com.ncmem.up6.utils.ConfigReader;

public class OSSConfig {
    public String ak="minioadmin";
    public String sk="minioadmin";
    public String endpoint="192.168.0.111:9000";
    public String bucket="test";

    public OSSConfig () {
        ConfigReader cr = new ConfigReader();
        this.ak = cr.readString("OSS.ak");
        this.sk = cr.readString("OSS.sk");
        this.endpoint = cr.readString("OSS.endpoint");
        this.bucket = cr.readString("OSS.bucket");
    }
}
