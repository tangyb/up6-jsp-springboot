package com.ncmem.up6.store;

import java.io.IOException;
import java.io.RandomAccessFile;

public class FileBlockReader {
    public StorageType storage=StorageType.IO;

    /**
     * 读取数据
     * @param pathSvr
     * @param offset
     * @param size
     * @return
     */
    public byte[] read(String pathSvr,long offset,long size) throws Exception {
        byte[] data = new byte[0];
        try {
            RandomAccessFile raf = new RandomAccessFile(pathSvr,"r");
            raf.seek(offset);
            data = new byte[(int)size];
            raf.read(data, 0, (int)size);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }//定位索引
        return data;
    }
}
