package com.ncmem.up6.model;

import java.util.UUID;

/**
 * Created by jmzy on 2021/1/5.
 */
public class DnFileInf extends  com.ncmem.up6.model.FileInf
{
    public static DnFileInf build(){return  new DnFileInf();}

    public DnFileInf(){}

    public String makeID(){
        String guid = UUID.randomUUID().toString();
        guid = guid.replace("-", "");
        return guid;
    }

    public String f_id = "";

    @DataBaseAttribute(name="f_fileUrl")
    public String fileUrl = "";

    @DataBaseAttribute(name="f_sizeSvr")
    public String sizeSvr = "0byte";

    @DataBaseAttribute(name="f_perLoc")
    public String perLoc = "0%";
}
