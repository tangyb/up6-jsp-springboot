package com.ncmem.up6.model;

import java.lang.annotation.*;

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataBaseAttribute {
    String name() default "";
}
