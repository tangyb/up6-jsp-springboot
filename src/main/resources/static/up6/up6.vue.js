﻿/*up6 vue.js 组件*/
Vue.component('up6', {
    template: '<div name="panel" class="up6-panel"></div>',
    props: { config: Object, fields: Object },
    data: function () {
        return {
            mgr: null
            , pluginInited: false
        }
    },//方法,
    methods: {
        //选择文件
        openFile: function () {
            this.mgr.openFile();
        },
        //选择目录
        openFolder: function () {
            this.mgr.openFolder();
        },
        //粘贴
        paste: function () {
            this.mgr.pasteFiles();
        },
        //清除已完成
        clear: function () {
            this.mgr.ClearComplete();
        },
        //安装控件
        setup: function () {
        },
        //任务队列是否为空
        taskEmpty: function () {
            return this.mgr.QueuePost.length < 1;
        },
        //全部停止
        taskEnd: function () {
            this.mgr.StopAll();
        },
        addFileLoc: function (v) {
            _this.mgr.addFileLoc(v);
        },
        addFolderLoc: function (v) {
            _this.mgr.addFolderLoc(v);
        }
    },
    mounted: function () {
        var _this = this;
        this.mgr = new HttpUploaderMgr({
            Config: _this.config,
            event: {
                //绑定事件
                md5Complete: function (obj, md5) {
                    _this.$emit("md5_complete", obj, md5);
                },
                scanComplete: function (obj) {
                    _this.$emit("scan_complete", obj);
                },
                fileAppend: function (obj) {
                    _this.$emit("file_append", obj);
                },
                fileComplete: function (obj) {
                    _this.$emit("file_complete", obj);
                },
                fdComplete: function (obj) {
                    _this.$emit("folder_complete", obj);
                },
                queueComplete: function () {
                    _this.$emit("queue_complete");
                },
                loadComplete: function () {
                    _this.pluginInited = true;
                    _this.$emit("load_complete");
                },
                unsetup: function (html) {
                    _this.$emit("unsetup", html);
                },
                itemSelected: function () {
                    _this.$emit('item_selected');
                }
            },
            ui: {
                render: $(".up6-panel")//加载到目标
            }
        });
    }
});