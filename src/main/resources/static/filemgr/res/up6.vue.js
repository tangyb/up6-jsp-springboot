﻿Vue.component('up6', {
    template: '<div name="files-panel" class="post-container">\
        <div class="post-item" name="file" >\
        <div class="img-box"><img name="file" :src="ico.file" /><img name="folder" :src="ico.folder" class="hide1" /></div>\
        <div class="area-l">\
            <div class="file-head">\
                <div name="name" class="name">HttpUploader程序开发.pdf</div>\
                <div name="percent" class="percent">(0%)</div>\
                <div name="size" class="size" child="1">0byte</div>\
            </div>\
            <div class="process-border"><div name="process" class="process"></div></div>\
            <div name="msg" class="msg top-space">15.3MB 20KB/S 10:02:00</div>\
        </div>\
        <div class="area-r">\
            <span class="img-btn hide1" name="post" title="继续"><img name="post" /><div>继续</div></span>\
            <span class="img-btn" name="cancel" title="取消"><img name="del" /><div>取消</div></span>\
            <span class="img-btn hide1" name="stop" title="停止"><img name="stop" /><div>停止</div></span>\
            <span class="img-btn hide1" name="del" title="删除"><img name="del" /><div>删除</div></span>\
        </div>\
    </div>\
        <div name="post_panel" class="post-container">\
            <span class="btn-t" name="selFile" style="display:none"><img name="postF" />上传文件</span>\
            <span class="btn-t" name="selFolder" style="display:none"><img name="postFd" />上传文件夹</span>\
            <span class="btn-t" name="paste" style="display:none"><img name="paste" />粘贴上传</span>\
            <span class="btn-t" name="clear" style="display:none"><img name="clear" />清除已完成</span>\
            <span class="btn-t" name="setup">安装控件</span>\
            <span class="btn-t" name="setupCmp"><img name="ok" />我已安装</span>\
            <div name="post_body" class="files-list"></div>\
        </div>\
</div>',
    props: { config: Object},
    data: function () {
        return {
            mgr: null
            , pluginInited: false
            , ico: {file:'/filemgr/res/imgs/32/file.png',folder:'filemgr/res/imgs/32/folder.png'}
        }
    },//事件,entLoadComplete
    methods: {
        //选择文件
        openFile: function () {
            this.mgr.openFile();
        },
        //选择目录
        openFolder: function () {
            this.mgr.openFolder();
        },
        //粘贴
        paste: function () {
            this.mgr.pasteFiles();
        },
        //任务队列是否为空
        taskEmpty: function () {
            return this.mgr.QueuePost.length < 1;
        },
        //全部停止
        taskEnd: function () {
            this.mgr.StopAll();
        },
        addFileLoc: function (v) {
            return this.mgr.addFileLoc(v);
        },
        addFolderLoc: function (v) {
            return this.mgr.addFolderLoc(v);
        }
    },
    mounted: function () {
        var _this = this;
        this.mgr = new HttpUploaderMgr({
            Config: _this.config,
            event: {
                //绑定事件
                md5Complete: function (obj, md5) {
                    _this.$emit("md5_complete", obj, md5);
                },
                scanComplete: function (obj) {
                    _this.$emit("scan_complete", obj);
                },
                fileAppend: function (obj) {
                    _this.$emit("file_append", obj);
                },
                fileComplete: function (obj) {
                    _this.$emit("file_complete", obj);
                },
                fdComplete: function (obj) {
                    _this.$emit("folder_complete", obj);
                },
                queueComplete: function () {
                    _this.$emit("queue_complete");
                },
                loadComplete: function () {
                    _this.pluginInited = true;
                    _this.$emit("load_complete");
                },
                unsetup: function (html) {
                    _this.$emit("unsetup", html);
                },
                itemSelected: function () {
                    _this.$emit('item_selected');
                }
            },
            ui: {
                render: $(".post-container")//加载到目标
            }
        });
    }
});