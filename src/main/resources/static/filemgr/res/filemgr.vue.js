﻿$(function () {

    v_app = new Vue({
        el: '#app',
        data: {
            items: [],
            count: 0,
            pageCurr: 1,
            pageLimit: 20,
            api: {
                data:root+"data",
                del:root+"del",
                del_batch:root+"del-batch",
                mkdir:root+"mk_folder",
                path:root+"path",
                rename:root+"rename",
                search:root+"search",
                tree:root+"tree.aspx",
                uncmp_down:root+"uncmp_down",
                uncomp:root+"uncomp"
            },
            config: {
                //up6配置
                up6: {
                    License2:"",
                    UrlCreate: root + "f_create",
                    UrlComplete: root + "f_complete",
                    UrlFdCreate: root + "fd_create",
                    UrlFdComplete: root + "fd_complete",
                    Cookie: "",
                    Fields: {uid:0}
                },
                //下载配置
                down2: {
                    License2:"",
                    UrlFdData: root + "fd_data",
                    Fields: { uid: 0 }
                }
            },
            //附加字段，业务参数
            fields: {uid:0},
            pathNav: [],
            pathCur: { id: "", pid: "", pidRoot: "", nameLoc: "根目录", pathRel: "/" },
            pathRoot: { id: "", pid: "", pidRoot: "", nameLoc: "根目录", pathRel: "/" },
            idSels: [],
            fileCur:null,
            toolbarCur:-1,
            idSelAll: false,
            folderMker: {name:'',edit:false},
            ico: {
                file: pathRes + 'imgs/16/file.png',
                folder: pathRes + 'imgs/16/folder.png',
                folder1: pathRes + 'imgs/16/folder1.png',
                btnUp: pathRes + "imgs/16/upload.png",
                btnUpFd: pathRes + "imgs/16/folder.png",
                btnPaste: pathRes + "imgs/16/paste.png",
                btnDel: pathRes + "imgs/16/del.png",
                btnDown: pathRes + "imgs/16/down.png",
                btnEdit: pathRes + "imgs/16/edit.png",
                btnPnlUp: pathRes + "imgs/16/up-panel.png",
                btnPnlDown: pathRes + "imgs/16/down-panel.png",
                ok: pathRes + "imgs/16/ok1.png",
                cancel: pathRes + "imgs/16/cancel.png"
            },
            cookie: "",
            search: {key:''}
        }
        , mounted: function () {
            this.pathNav.push(this.pathRoot);
        }
        , methods: {
            tm_format: function (v) {
                return moment(v).format('YYYY-MM-DD HH:mm:ss');
            }
            , trim: function (v) {
                v = v.replace(/^\s*|\s*$/gi, "");
                return v;
            }
            , init_data: function () {
                var _this = this;
                var param = $.extend({}, this.fields, { time: new Date().getTime() });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.data
                    , data: param
                    , success: function (res) {
                        _this.items = res.data;
                        _this.count = res.count;
                        _this.page_init();
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , btnUp_click: function () {
                this.$refs.up6.openFile();
            }
            , btnUpFolder_click: function () {
                this.$refs.up6.openFolder();
            }
            , btnPaste_click: function () {
                this.$refs.up6.paste();
            }
            , btnMkFolder_click: function () {
                this.folderMker.edit = true;
                var _this = this;
                setTimeout(function () {
                    _this.$refs.tbFdName.focus();
                 }, 10);
            }
            , btnMkFdOk_click: function () {
                var _this = this;
                this.folderMker.name = this.folderMker.name.replace(/^\s*|\s*$/gi, "");
                if (this.folderMker.name.length<1) {
                    layer.alert('文件夹名称不能为空！,', { icon: 2 });
                    return;
                }

                var param = $.extend({},this.fields, {
                    pid: this.pathCur.id,
                    pathRel: encodeURIComponent(this.pathCur.pathRel),
                    pidRoot: "",
                    nameLoc: encodeURIComponent(this.folderMker.name)
                });

                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.mkdir
                    , data: param
                    , success: function (res) {
                        if (!res.ret) {
                            layer.alert('创建失败,' + res.msg, { icon: 5 });
                        }
                        else {
                            var fd = $.extend(param,res,
                                {
                                    nameLoc:_this.folderMker.name,
                                    fdTask:true
                                });
                            _this.folderMker.name = '';
                            _this.folderMker.edit = false;
                            _this.items.unshift(fd);
                            _this.page_changed(1, 20);
                        }
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , btnMkFdCancel_click: function () {
                this.folderMker.edit = false;
                this.folderMker.name = '';
            }
            , btnOpenUp: function () { }
            , btnOpenDown: function () { }
            , btnDowns_click: function () {
                if (!this.$refs.down.check_path()) return;
                var _this = this;
                $.each(this.idSels, function (index, id) {
                    var arr = $.grep(_this.items, function (n, i) {
                        return n.id == id;
                    });
                    var f = arr[0];
                    var dt = {
                        f_id: f.id
                        , lenSvr: f.lenLoc
                        , pathSvr: f.pathSvr
                        , nameLoc: f.nameLoc
                        , object_key: f.object_key
                        , fileUrl: _this.$refs.down.mgr.Config["UrlDown"]
                    };
                    if (f.fdTask) {
                        _this.$refs.down.mgr.addFolder(dt);
                    }
                    else {
                        _this.$refs.down.mgr.addFile(dt);
                    }
                });
                this.openDown_click();
            }
            , btnDels_click: function () { }
            , btnDel_click: function (f) {
                var _this = this;
                layer.alert('确定要删除选中项：'+f.nameLoc+'？', {
                    time: 0 //不自动关闭
                    , btn: ['确定', '取消']
                    , yes: function (index) {
                        layer.close(index);

                        var param = jQuery.extend({}, {
                            id: f.id,
                            pathRel: encodeURIComponent(f.pathRel),
                            time: new Date().getTime()
                        });
                        $.ajax({
                            type: "GET"
                            , dataType: "json"
                            , url: _this.api.del
                            , data: param
                            , success: function (res) {
                                _this.page_changed(1, 20);
                            }
                            , error: function (req, txt, err) { }
                            , complete: function (req, sta) { req = null; }
                        });

                    }
                });
            }
            , btnSearch_click: function () {
                this.search.key = this.trim(this.search.key);

                if (this.search.key.length == 0) {
                    this.page_changed(1, this.pageLimit);
                    return;
                }

                var _this = this;
                var param = $.extend({}, this.fields, {
                    "page": 1,
                    limit: 100,
                    pid: this.pathCur.id,
                    pathRel: encodeURIComponent(this.pathCur.pathRel),
                    key: encodeURIComponent(this.search.key),
                    time: new Date().getTime()
                });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.search
                    , data: param
                    , success: function (res) {
                        _this.items = res.data;
                        _this.count = res.count;
                        _this.pageLimit = 100;
                        _this.page_init();
                        _this.pageLimit = 20;
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , searchKey_changed: function () {
                this.search.key = this.trim(this.search.key);
            }
            , selAll_click: function () {
                var _this = this;
                if (this.idSelAll) {
                    $.each(this.items, function (i, n) {
                        _this.idSels.push(n.id);
                    });
                }
                else {
                    this.idSels.length = 0;
                }
            }
            , openUp_click: function () {
                var _this = this;
                layer.open({
                    type: 1
                    , maxmin: true
                    , shade: 0//不显示遮罩
                    , title: '上传文件'
                    , offset: 'rb'//右下角
                    //, btn: ['确定', '取消']
                    , content: $("#pnl-up")
                    , area: ['452px', '562px']
                    , success: function (layero, index) {
                        $(_this.$refs.up6).show();
                    }
                    , btn1: function (index, layero) {
                        layer.close(index);//关闭窗口
                        $(_this.$refs.up6).hide();
                    }
                    , btn2: function (index, layero) {
                        $(_this.$refs.up6).hide();
                    }
                });
            }
            , openDown_click: function () {
                var _this = this;
                layer.open({
                    type: 1
                    , maxmin: true
                    , shade: 0//不显示遮罩
                    , title: '文件下载'
                    , offset: 'rb'//右下角
                    //, btn: ['确定', '取消']
                    , content: $("#pnl-down")
                    , area: ['452px', '562px']
                    , success: function (layero, index) {
                        $(_this.$refs.down).show();
                    }
                    , btn1: function (index, layero) {
                        layer.close(index);
                        $(_this.$refs.down).hide();                        
                    }
                    , btn2: function (index, layero) {
                        $(_this.$refs.down).hide();                        
                    }
                });
            }
            , nav_click: function (p) {
                var _this = this;
                this.idSels.length = 0;
                this.idSelAll = false;
                //加载路径
                var param = $.extend({},this.fields, p,{time: new Date().getTime() });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.path
                    , data: { data: encodeURIComponent(JSON.stringify(param)), pathRel:encodeURIComponent(p.pathRel) }
                    , success: function (res) {
                        _this.pathNav = res;
                        _this.path_changed(p);
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , open_tree: function (d) { }
            , open_folder: function (d) {
                if (!d.fdTask) return;

                this.nav_click(d);
            }
            , path_changed: function (d) {
                var _this = this;
                $.extend(this.pathCur, d);
                //加载文件列表
                var param = $.extend({}, this.fields, {
                    pid: d.id,
                    pathRel: encodeURIComponent( d.pathRel),
                    time: new Date().getTime()
                });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.data
                    , data: param
                    , success: function (res) {
                        _this.items = res.data;
                        _this.count = res.count;
                        _this.page_init();
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });

            }
            , page_init: function () {
                var _this = this;
                layui.use('laypage', function () {
                    var laypage = layui.laypage;

                    laypage.render({
                        elem: 'pager' //
                        , count: _this.count//
                        , layout: ['prev', 'page', 'next', 'limit', 'count', 'skip']
                        , limit: _this.pageLimit
                        , curr: _this.pageCurr
                        , jump: function (obj, first) {
                            //首次不执行
                            if (!first) {
                                _this.pageLimit = obj.limit;
                                _this.pageCurr = obj.curr;
                                _this.page_changed(obj.curr, obj.limit);
                            }
                        }
                    });
                });
            }
            , page_changed: function (page,size) {
                var _this = this;
                var param = $.extend({}, this.fields, {
                    "page": page,
                    limit: size,
                    pid: this.pathCur.id,
                    pathRel: encodeURIComponent(this.pathCur.pathRel),
                    key: encodeURIComponent( this.search.key),
                    time: new Date().getTime()
                });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.data
                    , data: param
                    , success: function (res) {
                        _this.items = res.data;
                        _this.count = res.count;
                        _this.page_init();
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , itemDown_click: function (f) {
                this.fileCur=f;
                if (!this.$refs.down.check_path()) return;
                var _this = this;
                var dt = {
                    f_id: f.id
                    , lenSvr: f.lenLoc
                    , pathSvr: f.pathSvr
                    , nameLoc: f.nameLoc
                    , object_key: f.object_key
                    , fileUrl: this.$refs.down.mgr.Config["UrlDown"]
                };
                if (f.fdTask) _this.$refs.down.mgr.addFolder(dt);
                else _this.$refs.down.mgr.addFile(dt);
                this.openDown_click();
            }
            , itemRename_click: function (f, i) {
                //$.extend(f, { edit: true });
                $("div[name='v" + i + "']").hide();
                $("input[name='name" + i + "']").show().val(f.nameLoc);
                $("div[name='edit" + i + "']").show();
            }
            , btnRename_ok: function (f, i) {
                var nameNew = $("input[name='name" + i + "']").val();
                nameNew = nameNew.replace(/^\s*|\s*$/gi, "");
                if (nameNew.length<1) {
                    layer.alert('名称不能为空', { icon: 2 });
                    return;
                }
                var _this = this;
                var param = $.extend({}, f,{nameLoc: encodeURIComponent(nameNew),times: new Date().getTime() });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.rename
                    , data: param
                    , success: function (res) {
                        if (!res.state) {
                            layer.alert('重命名失败,' + res.msg, { icon: 5 });
                        }
                        else
                        {
                            f.nameLoc = nameNew;
                            f.pathRel = res.pathRel;
                            _this.btnRename_cancel(f, i);
                        }
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , btnRename_cancel: function (f, i) {
                $("div[name='v" + i + "']").show();
                //$("input[name='" + name + "']").show().val(f.nameLoc);
                $("div[name='edit" + i + "']").hide();
            }
            , toolbar_enter: function (e, index) {
                this.toolbarCur = index;
            }
            , toolbar_leave: function (e, index) {
                this.toolbarCur = -1;
            }
            , up6_loadComplete: function () {
                this.up6_loadTask();
            }
            , up6_itemSelected: function () {
                this.openUp_click();
            }
            , up6_fileAppend: function (f) {
                //为文件设置当前位置
                $.extend(f.fields, { pid: this.pathCur.id, pidRoot: "",pathRel:encodeURIComponent(this.pathCur.pathRel) });
            }
            , up6_fileComplete: function (f) {
                this.page_changed(1, 20);
            }
            , up6_folderComplete: function (f) {
                this.page_changed(1, 20);
            }
            , up6_unsetup:function(html){
                layer.open({
                    title: '提示',
                    content: html
                  });
            }
            , up6_loadTask: function () {
                var _this = this;
                var param = jQuery.extend({}, this.fields, { time: new Date().getTime() });
                $.ajax({
                    type: "GET"
                    , dataType: "json"
                    , url: this.api.uncomp
                    , data: param
                    , success: function (res) {
                        if (res.length > 0) _this.openUp_click();

                        $.each(res, function (i, n) {
                            if (n.fdTask) {
                                var f = _this.$refs.up6.addFolderLoc(n);
                                f.folderInit = true;
                                f.folderScan = true;
                                f.ui.btn.post.show();
                                f.ui.btn.del.show();
                                f.ui.btn.cancel.hide();
                            }
                            else {
                                var f = _this.$refs.up6.addFileLoc(n);
                                f.ui.percent.text("(" + n.perSvr + ")");
                                f.ui.process.css("width", n.perSvr);
                                f.ui.btn.post.show();
                                f.ui.btn.del.show();
                                f.ui.btn.cancel.hide();
                            }
                        });
                    }
                    , error: function (req, txt, err) { }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , down_loadComplete: function () {
                this.down_loadTask();
            }
            , down_sameFileExist: function (n) { }
            , down_unsetup:function(html){
                layer.open({
                    title: '提示',
                    content: html
                  });
            }
            , down_loadTask: function () {
                var _this = this;
                //加载未完成的任务
                var param = $.extend({}, this.$refs.down.mgr.Config.Fields);
                $.ajax({
                    type: "GET"
                    , dataType: 'json'
                    , jsonp: "callback" //自定义的jsonp回调函数名称，默认为jQuery自动生成的随机函数名
                    , url: this.api.uncmp_down
                    , data: param
                    , success: function (files) {
                        if (files.length > 0) { _this.openDown_click(); }
                        $.each(files, function (i, n) {
                            var dt = $.extend({},n,{svrInit: true});
                            _this.$refs.down.mgr.resume_file(dt);
                        });
                    }
                    , error: function (req, txt, err) { alert("加载文件列表失败！" + req.responseText); }
                    , complete: function (req, sta) { req = null; }
                });
            }
            , down_folderSel : function(){                
                var _this = this;
                setTimeout(function(){
                    if(_this.fileCur!=null) _this.itemDown_click(_this.fileCur);
                    _this.btnDowns_click();
                    _this.fileCur = null;
                },1000);
                this.openDown_click();
            }
            , taskEmpty: function () {
                var ept = this.$refs.up6.taskEmpty();
                if (ept) ept = this.$refs.down.taskEmpty();
                return ept;
            }
            , taskEnd: function () {
                this.$refs.up6.taskEnd();
                this.$refs.down.taskEnd();
            }
        }
    });
    v_app.init_data();
});